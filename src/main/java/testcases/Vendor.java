package testcases;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class Vendor {
	
	@Test
	public void vendor() throws InterruptedException {
		
		//Set Property for Chrome browser 
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		//Lanuch chrome browser
		ChromeDriver driver = new ChromeDriver();
		
		//Enter URL
		driver.get("https://acme-test.uipath.com/account/login");
		
		//Maximize window 
		driver.manage().window().maximize();
		
		//Click Log In
		driver.findElementByLinkText("Log In").click();
		
		//Enter Email
		driver.findElementByXPath("//input[@id='email']").sendKeys("charan.laavan@gmail.com");
		
		//Enter Password
		driver.findElementByXPath("//input[@id='password']").sendKeys("Redwin*3159");
		
		//Click on Log In 
		driver.findElementByXPath("//button[@id='buttonLogin']").click();
		
		Thread.sleep(1000);
		
		//Action class 
		Actions builder = new Actions(driver);
		WebElement ele = driver.findElementByXPath("//button[text()=' Vendors']");
		builder.moveToElement(ele).pause(2000).perform();
		driver.findElementByLinkText("Search for Vendor").click();
		
		//Enter Vendor Id 
		driver.findElementById("vendorTaxID").sendKeys("DE767565");
		
		//Click on Search 
		driver.findElementById("buttonSearch").click();
		
		//Find the table 
		WebElement table = driver.findElementByClassName("table");
		
		//Find Rows 
		List<WebElement> rows = driver.findElementsByTagName("tr");
		
		//Row size
		System.out.println(rows.size());
		
		//Get first row 
		WebElement firstRow = rows.get(1);	
		
		//Find Columns 
		List<WebElement> columns = firstRow.findElements(By.tagName("td"));
		
		//Print VendorName 
		String vendorName = columns.get(0).getText();
		
		System.out.println("Vendor Name : "+vendorName);
		
	
		driver.close();
		
		
	}

}
